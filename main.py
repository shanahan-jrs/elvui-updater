#!/usr/bin/env python3
# coding: utf-8

import shutil
import os
import zipfile

import requests
from bs4 import BeautifulSoup

TUKUI_DOMAIN = 'https://www.tukui.org'
ADDONS_PATH = '/mnt/d/john/BlizzardStuff/World Of Warcraft/interface/addons'
ELVUI_DIR_NAME = 'ElvUI'
ELVUI_CONFIG_DIR_NAME = 'ElvUI_Config'
TEMP_DIR = '/mnt/c/Windows/Temp'
TEMP_FILENAME = 'elvui_download.zip'
EXTRACTED_FILENAME = 'elvui'

def get_download_page_html(url='https://tukui.org/download.php?ui=elvui'):
    return requests.get(url).text

def parse_download_page(html):
    return BeautifulSoup(html, "html.parser")

def get_download_link_from_parsed_html(parsed_html):
    href = parsed_html.find('div', {'id': 'download'}).find('a', {'class': 'btn btn-mod btn-border-w btn-round btn-large'})['href']
    return TUKUI_DOMAIN +  href

def download_compressed_elvui(url):
    download_path = TEMP_DIR + '/' + TEMP_FILENAME

    # remove old compressed file if it exists still
    if os.path.exists(download_path):
        os.remove(download_path)

    req = requests.get(url)
    with open(download_path, 'wb') as f:
        f.write(req.content)

def unzip_compressed_elvui():
    compressed_file_path = TEMP_DIR + '/' + TEMP_FILENAME
    extracted_file_path = TEMP_DIR + '/' + EXTRACTED_FILENAME

    # remove old extracted file if it exists still
    if os.path.exists(extracted_file_path):
        shutil.rmtree(extracted_file_path, ignore_errors=True)

    zip_obj = zipfile.ZipFile(compressed_file_path, 'r')
    zip_obj.extractall(extracted_file_path)
    zip_obj.close()

def remove_currently_installed_elvui():
    elvui_path = ADDONS_PATH + '/' + ELVUI_DIR_NAME
    elvui_config_path = ADDONS_PATH + '/' + ELVUI_CONFIG_DIR_NAME

    if os.path.exists(elvui_path):
        shutil.rmtree(elvui_path, ignore_errors=True)
    if os.path.exists(elvui_config_path):
        shutil.rmtree(elvui_config_path, ignore_errors=True)

def install_new_elvui():
    elvui_tmp_path = TEMP_DIR + '/' + EXTRACTED_FILENAME + '/' + ELVUI_DIR_NAME
    elvui_config_tmp_path = TEMP_DIR + '/' + EXTRACTED_FILENAME + '/' + ELVUI_CONFIG_DIR_NAME

    elvui_install_path = ADDONS_PATH + '/' + ELVUI_DIR_NAME
    elvui_config_install_path = ADDONS_PATH + '/' + ELVUI_CONFIG_DIR_NAME

    shutil.copytree(elvui_tmp_path, elvui_install_path)
    shutil.copytree(elvui_config_tmp_path, elvui_config_install_path)

def clean_up():
    os.remove(TEMP_DIR + '/' + TEMP_FILENAME)
    shutil.rmtree(TEMP_DIR + '/' + EXTRACTED_FILENAME, ignore_errors=True)

def _main():
    print('Starting ElvUI updater...')

    # Grab page source
    print('Pulling the TukUI download page...')
    download_page_html = get_download_page_html()

    # Parse page source
    print('Parsing download page html...')
    parsed_download_page = parse_download_page(download_page_html)

    # Pull download link from parsed html
    print('Extracting download link from the page...')
    download_link = get_download_link_from_parsed_html(parsed_download_page)

    # Download the actual elvui zip
    print('Downloading the ElvUI zip file...')
    download_compressed_elvui(download_link)

    # Unzip file
    print('Unzipping ElvUI...')
    unzip_compressed_elvui()

    # Remove old elvui install
    print('Removing old ElvUI installation...')
    remove_currently_installed_elvui()

    # Copy new elvui to the addons dir
    print('Installing new version of ElvUI...')
    install_new_elvui()

    # Clean up tmp crap
    print('Cleaning up tmp files...')
    clean_up()

    print('Done. Restart WoW client.')

if __name__ == "__main__":
    _main()
